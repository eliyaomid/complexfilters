$(document).ready(function() {
  var options = {
    startNode: 'queryStart',
    grammar: {
      'queryStart': {
        label: false,
        children: ['$Columns'],
        widget: 'textWidget'
      },
      'time': {
        label: 'Time Column',
        field: 'column',
        group: 'Columns',
        children: ['$Operators'],
        widget: 'textWidget'
      },
      'text': {
        label: 'Text Column',
        field: 'column',
        group: 'Columns',
        children: ['$Operators'],
        widget: 'textWidget'
      },
      'key-value': {
        label: 'Key Value Column',
        field: 'column',
        group: 'Columns',
        children: ['$Keys'],
        widget: 'textWidget'
      },
      'key': {
        label: 'Key',
        field: 'key',
        group: 'Keys',
        children: ['$Operators'],
        widget: 'textWidget'
      },
      'eq': {
        label: '&#61;',
        field: 'operator',
        group: 'Operators',
        children: ['value'],
        widget: 'textWidget'
      },
      'neq': {
        label: '&#8800;',
        field: 'operator',
        group: 'Operators',
        children: ['value'],
        widget: 'textWidget'
      },
      'value': {
        label: false,
        field: 'value',
        validator: function(value) {return true;},
        final: true,
        children: ['queryStart']
      }
    },
    validateGrammar: function(options) {
      try {
        var opratorState = options.inputState[options.inputState.length - 1];
        var columnState = options.inputState[options.inputState.length - 2];
        if (columnState) {
          var opratorNode = options.grammar[opratorState.key];
          if (columnState.key == 'time') {
            opratorNode.widget = 'timeWidget';
          } else {
            opratorNode.widget = 'textWidget';
            
          }
        }
      } catch(err) {
        console.log(err);
      }
      return options.grammar;
    }
  };

  $("#filter").complexfilters(options);
});
