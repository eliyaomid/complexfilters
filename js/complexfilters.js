(function ( $ ) {

  var inputWidget = {
    textWidget: {
      widgetInputElement: null,
      render: function( initialValue ) {
        this.widgetInputElement = $('<input type="text"/>');
        if (initialValue != undefined) {
          this.widgetInputElement.val(initialValue);
        }
        return this.widgetInputElement;
      },
      val: function( initialValue ) {
        if (initialValue == undefined) {
          return this.widgetInputElement.val();
        } else {
          this.widgetInputElement.val(initialValue);
        }
      },
      reset: function() {
        this.widgetInputElement.val("");
      },
      remove: function() {
        this.widgetInputElement.remove();
      },
      focus: function() {
        this.widgetInputElement.focus();
      }
    },
    numberWidget: {
      widgetInputElement: null,
      render: function( initialValue ) {
        this.widgetInputElement = $('<input type="number"/>');
        if (initialValue != undefined) {
          this.widgetInputElement.val(initialValue);
        }
        return this.widgetInputElement;
      },
      val: function( initialValue ) {
        if (initialValue == undefined) {
          return this.widgetInputElement.val();
        } else {
          this.widgetInputElement.val(initialValue);
        }
      },
      reset: function() {
        this.widgetInputElement.val("");
      },
      remove: function() {
        this.widgetInputElement.remove();
      },
      focus: function() {
        this.widgetInputElement.focus();
      }
    },
    timeWidget: {
      widgetInputElement: null,
      render: function( initialValue ) {
          this.widgetInputElement = $('<input type="time"/>');
        if (initialValue != undefined) {
          this.widgetInputElement.val(initialValue);
        }
        return this.widgetInputElement;
      },
      val: function( initialValue ) {
        if (initialValue == undefined) {
          return this.widgetInputElement.val();
        } else {
          this.widgetInputElement.val(initialValue);          
        }
      },
      reset: function() {
        this.widgetInputElement.val("");
      },
      remove: function() {
        this.widgetInputElement.remove();
      },
      focus: function() {
        this.widgetInputElement.focus();
      }
    }
  };
  var settings = {};
  var container = null;
  var inputElement = null;
  var inputWidgetObject = null;
  var filterBox = null;
  var inputBox = null;
  var suggestions = null;
  var defaultNode = {
    label: false,
    decorate: null,
    group: undefined,
    field: undefined,
    children: [],
    final: false,
        widget: undefined,
        validator: function( value ){ return false; },
  };
  var validGrammar = {};
  var currentNode = null;
  var inputState = [];
  var activeToken = null;
  var groupHash = null;

  $.fn.complexfilters = function( options ) {
    var defaults = {
      containerClass: "complexfilters-container",
      filterBoxClass: "filter-box",
      inputBoxClass: "input-box",
      suggestionsClass: "suggestion-drop",
      tokenItemClass: "token-item",
      tokenItemCloseClass: "token-item-close",
      tempTokenItemClass: "temp-token-item",
      suggesteditemClass: "suggested-item",
      suggestedValueClass: "suggested-value",
      suggestedNodeClass: "node-value",
      suggestedGroupItemClass: "group-item",
      suggestedGroupClass: "group-value",
      startNode: null,
      grammar: {},
      validateGrammar: function(options) {return options.grammar;}
    }
    settings = $.extend( {}, defaults, options );

    inputElement = this;
    init();
    start(settings.startNode, false);
    return this;
  };

  /*
   * Check grammar with existing tokens and return valid grammar
   */
  function getGrammar(force=false) {
    if (force) {
      var options = {
        grammar: settings.grammar,
        inputState: inputState
      } 
      var validatedGrammar = settings.validateGrammar(options);
      makeGroupHash(validatedGrammar);
      validatedGrammar = makeGroupChildren(validatedGrammar);
      validGrammar = validatedGrammar;
    }
    return validGrammar;
  };

  function makeGroupHash(validatedGrammar) {
    groupHash = {
      "_": []
    };
    for (node in validatedGrammar) {
      var grammarNode = validatedGrammar[node];
      var grammarNodeLabel = getHybrid(grammarNode.label);
      var groupItem = {
        label: grammarNodeLabel,
        value: node
      };
      if (grammarNodeLabel) {
        if (grammarNode.group == undefined) {
          groupHash["_"].push(groupItem);
        } else {
          if (grammarNode.group in groupHash) {
            groupHash[grammarNode.group].push(groupItem);
          } else {
            groupHash[grammarNode.group] = [groupItem];
          }
        }
      }
    }
  };

  function makeGroupChildren(validatedGrammar) {
    for (node in validatedGrammar) {
      var grammarNode = validatedGrammar[node];
      var realChildren = [];
      for (let child of grammarNode.children) {
        if (child[0] == '$' && child[1] != '$') {
              if (child.substr(1) in groupHash) {
                  var groupFields = groupHash[child.substr(1)];
                  for (let field of groupFields) {
                    realChildren.push(field.value);
                  }
              }
            } else {
              realChildren.push(child);
            }
      }
      validatedGrammar[node]["children"] = realChildren;
    }
    return validatedGrammar
  }

  /*
   * Make complexfilter widger initial ui
   */
  function init() {
    inputElement.hide();
    container = $( '<div>' ).addClass(settings.containerClass);

    filterBox = $( '<ul>' ).addClass(settings.filterBoxClass);
    filterBox.on("click", "." + settings.tokenItemClass, filterBoxEventHandler);

    inputBox = $('<li>').addClass(settings.inputBoxClass);
    filterBox.append(inputBox);
    container.append(filterBox);

    suggestions = $('<ul>').addClass(settings.suggestionsClass);
    suggestions.on("mousedown", "." + settings.suggesteditemClass, suggestionsEventHandler);
    container.append(suggestions);
    inputElement.after(container);

    //Extend default grammar node 
    for (node in settings.grammar) {
      var grammarNode = settings.grammar[node];
      settings.grammar[node] = $.extend( {}, defaultNode, grammarNode );
    }
  };

  function start(node, inputWidgetFocus=true) {
    var grammar = getGrammar(true);

    if (jQuery.isEmptyObject(grammar)) {
      inputBox.hide();
    }
    currentNode = grammar[node];

    if (currentNode.final == true) {
      if (currentNode.children.length == 0) {
        submit();
      } else if (currentNode.children.length == 1) {
        start(currentNode.children[0]);
      }
    } else if (currentNode.widget != undefined) {
      terminateInputWidget();
      inputWidgetObject = inputWidget[currentNode.widget];
      var inputWidgetElement = inputWidgetObject.render();
      inputWidgetElement.on("keydown keyup keypress click blur focus", inputWidgetEventHandler);
      inputBox.append(inputWidgetElement);
      if (inputWidgetFocus) {
        inputWidgetElement.focus();
      }
    }
    //Make suggestion
    makeSuggestion();

    inputElement.attr("value", val());
  };

  function makeSuggestion(widgetValue, acceptValue=false, isNode) {
    //Remove old suggestion
    suggestions.html("");
    var grammar = getGrammar();

    //Make suggested value
    if (acceptValue === true) {
      var addOptionValue = "";
      if (activeToken) {
        $.each(activeToken.find("span"), function() {
          addOptionValue += " " + $(this).html();
        });
      }
      if (isNode === true) {
        var grammarNode = grammar[widgetValue];
        if (grammarNode.final === true) {
          var grammarNodeLabel = getHybrid(grammarNode.label);
          if (grammarNodeLabel) {
            addOptionValue += " " + grammarNodeLabel;
          }
        } else {
          addOptionValue = "";
        }
      } else {
        addOptionValue += " " + widgetValue;
      }
      if (addOptionValue) {
        var suggestedValue = $('<li>').
          addClass(settings.suggesteditemClass).
          addClass(settings.suggestedValueClass).
          attr("value", widgetValue).html("Add option: " + addOptionValue);
        suggestions.append(suggestedValue);
      }
    }

    // "_" group name for ungrouped node
    var suggestedItemList = {
      "_": []
    };
    for (let child of currentNode.children) {
      var isSimilar = true;
      var grammarNode = grammar[child];
      var grammarNodeLabel = getHybrid(grammarNode.label);
      if (grammarNodeLabel) {
        var groupItem = {
          label: grammarNodeLabel,
          value: child
        };
        if (widgetValue) {
          if (isMatchItem(widgetValue, grammarNodeLabel, child)) {
            var groupName = grammarNode.group == undefined? "_": grammarNode.group;
            suggestedItemList = {};
            suggestedItemList[groupName] = [groupItem];
            break;
          } else if (!isSimilarItem(widgetValue, grammarNodeLabel, child)) {
            isSimilar = false;
          }        
        }
        if (isSimilar) {
          if (grammarNode.group == undefined) {
            suggestedItemList["_"].push(groupItem);
          } else {
            if (grammarNode.group in suggestedItemList) {
              suggestedItemList[grammarNode.group].push(groupItem);
            } else {
              suggestedItemList[grammarNode.group] = [groupItem];
            }
          }
        }
      }
    }

    for (groupName in suggestedItemList) {
      if (groupName != "_") {
        var suggestedGroup = $('<li>').
          addClass(settings.suggesteditemClass).
          addClass(settings.suggestedGroupClass).
          html(groupName);
        suggestions.append(suggestedGroup);        
      }
      var groupChildList = suggestedItemList[groupName];
      for (let groupChild of groupChildList) {
        var suggestedNode = $('<li>').
          addClass(settings.suggesteditemClass).
          addClass(settings.suggestedNodeClass).
          attr("value", groupChild.value).html(groupChild.label);
        if (groupName != "_") {
          suggestedNode.addClass(settings.suggestedGroupItemClass);
        }
        suggestions.append(suggestedNode);
      }
    }
  };

  function isMatchItem(widgetValue, grammarNodeLabel, grammerNodeKey) {
    var isMatchItem = false;
    if (widgetValue.toLowerCase() == grammarNodeLabel.toLowerCase()) {
      isMatchItem = true;
    } else if (widgetValue.toLowerCase() == grammerNodeKey.toLowerCase()) {
      isMatchItem = true;
    }
    return isMatchItem;
  };

  function isSimilarItem(widgetValue, grammarNodeLabel, grammerNodeKey) {
    var isSimilarItem = false;
    if (grammarNodeLabel.toLowerCase().indexOf(widgetValue.toLowerCase()) >= 0) {
      isSimilarItem = true;
    } else if(grammerNodeKey.toLowerCase().indexOf(widgetValue.toLowerCase()) >= 0) {
      isSimilarItem = true;
    }
    return isSimilarItem;
  };

  function submit() {
    //Make token item from temp token
    activeToken.data("json-value", inputState);
    //Set token close icon
    var activeTokenClose = $('<a>').addClass(settings.tokenItemCloseClass).html(" x");
    activeTokenClose.on("click", closeTokenItemEventHandler);
    activeToken.append(activeTokenClose);
    //Remove temp token class and add token class
    activeToken.removeClass(settings.tempTokenItemClass).addClass(settings.tokenItemClass);
    //Terminate active token
    activeToken = null;

    //Terminate input state stack
    inputState = [];
  };

  /*
   * Terminate input widget
   */
  function terminateInputWidget() {
    if (inputWidgetObject) {
      inputWidgetObject.remove();
      inputWidgetObject = null;
    }
  };

  function filterBoxEventHandler(event) {
    var jsonValue = $(this).data("json-value");
    for (let value of jsonValue) {
      var nextNode = pushState(value.value);
      if (nextNode) {
        start(nextNode);
      }
    }
    $(this).find("." + settings.tokenItemCloseClass).click();
  }

  /*
   * Close token that click on close icon
   * Handle click
   */
  function closeTokenItemEventHandler(event) {
    $(this).parent("." + settings.tokenItemClass).remove();
  };

  /*
   * Handle event on input widget
   * keypress, keyup, click , ...
   */
  function inputWidgetEventHandler(event) {
    var widgetValue = null;
    if (currentNode.widget != undefined) {
      widgetValue = inputWidgetObject.val();
    }
    if(event.type == "keydown") {
      if (isBackspaceKey(event.keyCode)) {
        if (widgetValue == "" && activeToken) {
          goBack();
          return true;
        }
      }
    } else if (event.type == "keyup") {
      event.preventDefault();
      if (isEditKey(event.keyCode)) {
        var acceptResult = isAcceptInGrammar(currentNode, widgetValue);
        if (acceptResult.accept === true) {
          makeSuggestion(widgetValue, true, acceptResult.isNode);
        } else {
          makeSuggestion(widgetValue);
        }
      }
    } else if (event.type == "keypress") {
      //if keypress is enter and widget value in children
      if (isAcceptKey(event.which)) {
        event.preventDefault();
        var nextNode = pushState(widgetValue);
        if (nextNode) {
          start(nextNode);
        }
        return true;
      }
    } else if (event.type == "focus") {
      //Show suggestions dropdown
      suggestions.show();
    } else if (event.type == "blur") {
      //Hide suggestions dropdown
      suggestions.hide();
    }
  };

  /*
   * Handle event on suggestion item
   * mousedown
   */
  function suggestionsEventHandler(event) {
    event.preventDefault();
    if ($(this).hasClass(settings.suggestedGroupClass)) {
      return;
    } else if ($(this).hasClass(settings.suggestedValueClass)) {
      var nextNode = pushState($(this).attr("value"));
      if (nextNode) {
        submit();
        start(nextNode);
      }
    } else {
      var nextNode = pushState($(this).attr("value"));
      if (nextNode) {
        start(nextNode);
      }
    }
  }

  function goBack() {
    var startNode = settings.startNode;
    var grammar = getGrammar();
    activeToken.find("span").last().remove();
    //Pop current state from input state
    inputState.pop();
    if (inputState.length > 0) {
      var lastState = inputState[inputState.length-1];
      startNode = lastState.key;
    } else {
      activeToken.remove();
      activeToken = null;
    }
    start(startNode);
  };

  /*
   * Push current widget value
   * if widget value accepted
   */
  function pushState(widgetValue) {
    var grammar = getGrammar();
    var widgetValueLabel = widgetValue;
    var startNode = settings.startNode;
    var outValue = {
      field: null,
      value: null,
      key: null
    };

    var acceptResult = isAcceptInGrammar(currentNode, widgetValue);
    if (acceptResult.accept === true) {
      if (acceptResult.isNode === true) {
        var nodeLabel = grammar[acceptResult.gotoNode].label;
        nodeLabel = getHybrid(nodeLabel);
        if (nodeLabel) {
          widgetValueLabel = nodeLabel;
        }
      }
      outValue.value = widgetValue;
      outValue.key = acceptResult.gotoNode;
      outValue.field = grammar[acceptResult.gotoNode].field;
      startNode = acceptResult.gotoNode;
    } else {
      return false;
    }
    if (inputState.length == 0) {
      activeToken = $('<li>').addClass(settings.tempTokenItemClass);
      inputBox.before(activeToken);
    }
    activeToken.append($('<span>').html(widgetValueLabel));
    if (currentNode.widget != undefined) {
      inputWidgetObject.reset();
    }
    inputState.push(outValue);

    return startNode;
  };

  function val(value) {
    if (value === undefined) {
      var returnValue = [];
        $.each(filterBox.find("li." + settings.tokenItemClass), function() {
          var dataValue = $(this).data("json-value");
          returnValue.push(dataValue);
        });
        return JSON.stringify(returnValue);
    } else {
      //@TODO: Get JSON value and make tokens    
    }
  }
  /*
   * Check given value accept grammar on this node
   */
  function isAcceptInGrammar(node, value) {
    var acceptResult = {
      accept: false,
      gotoNode: null,
      isNode: false
    }
    var grammar = getGrammar();
    if (node.children.indexOf(value) >= 0) {
      acceptResult.accept = true;
      acceptResult.isNode = true;
      acceptResult.gotoNode = value;
    } else {
      for (let child of node.children) {
        var grammarNode = grammar[child];
        if (grammarNode == undefined) {
          console.log("unsupported node in children!");
        } else {
          if (grammarNode.validator != undefined) {
            if (getHybrid(grammarNode.validator, value) === true) {
              acceptResult.accept = true;
              acceptResult.gotoNode = child;
              break;
            }
          }
        }
      }
    }
    return acceptResult;
  };

  function getHybrid(obj, value) {
    if (obj instanceof RegExp) {
      return obj.test(value);
    }
    return (typeof obj == "function")? obj(value): obj;
  };

  function isAcceptKey(key) {
    return key in { 9: "Tab", 13: "Enter" };
  };

  function isEditKey(key) {
    if (key in { 8: "Backspace", 46: "Delete" })
      return true;
    else if (key >= 32 && key <= 126) //Characters
      return true;

    return false;
  };

  function isBackspaceKey(key) {
    if (key in { 8: "Backspace" })
      return true;
  };

}( jQuery ));
